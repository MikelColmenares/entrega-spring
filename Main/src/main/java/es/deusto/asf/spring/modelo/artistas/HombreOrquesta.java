package es.deusto.asf.spring.modelo.artistas;

import es.deusto.asf.spring.modelo.ActuacionException;
import es.deusto.asf.spring.modelo.instrumentos.Instrumento;

import java.util.Collection;

public class HombreOrquesta extends ArtistaImpl {

    private Collection<Instrumento> instrumentos;

    @Override
    public void actuar() throws ActuacionException {
        System.out.println("Soy un hombre orquesta y voy a tocar varios instrumentos:");
        for (Instrumento instrumento:instrumentos
             ) {
            instrumento.tocar();
        }
    }

    public HombreOrquesta (String nombre, String actuacion){
        super(nombre, actuacion);
    }

    public void setInstrumentos(Collection<Instrumento> instrumentos) {
        this.instrumentos = instrumentos;
    }
}
