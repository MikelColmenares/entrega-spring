package es.deusto.asf.spring.modelo.instrumentos;

public class Bateria implements Instrumento{

    @Override
    public void tocar() {
        System.out.println("TUPA TUPA TUPA");
    }
}
