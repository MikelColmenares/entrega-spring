package es.deusto.asf.spring.modelo.mentalistas;

public class Voluntario implements Pensador {

    private String pensamientos;

    @Override
    public void pensarSobreAlgo(String pensamientos) {
        this.pensamientos = pensamientos;
    }

    public String getPensamientos() {
        return pensamientos;
    }

    public Voluntario() {
    }
}
