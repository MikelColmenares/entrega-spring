package es.deusto.asf.spring.modelo.dao;

import es.deusto.asf.spring.modelo.ActuacionException;
import es.deusto.asf.spring.modelo.artistas.Artista;

public interface ArtistaDAOInterf {
    public void insert(Artista artista);
}
