package es.deusto.asf.spring.modelo;

import es.deusto.asf.spring.modelo.artistas.Artista;
import es.deusto.asf.spring.modelo.artistas.HombreOrquesta;
import es.deusto.asf.spring.modelo.dao.ArtistaDAO;
import es.deusto.asf.spring.modelo.instrumentos.Instrumento;
import es.deusto.asf.spring.modelo.mentalistas.Mentalista;
import es.deusto.asf.spring.modelo.mentalistas.Pensador;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ConcursoDeTalentosMain {

    public static void main(String args[]){
        ApplicationContext context = new ClassPathXmlApplicationContext("concursoDeTalentos.xml");

        List<Artista> artistas = new ArrayList<>();
        Collection<Instrumento> instrumentos = new ArrayList<>();
        //artistas.add((Artista)context.getBean("aitor"));
        //artistas.add((Artista)context.getBean("josu"));
        artistas.add((Artista)context.getBean("andrea"));
        artistas.add((Artista)context.getBean("unai"));
        artistas.add((Artista)context.getBean("andoni"));
        instrumentos.add((Instrumento) context.getBean("piano"));
        HombreOrquesta hombreOrquesta = (HombreOrquesta) context.getBean("aitor");
        hombreOrquesta.setInstrumentos(instrumentos);
        try{
            hombreOrquesta.actuar();
        } catch (ActuacionException a) {
            System.out.println("Fallo en la actuación");
        }
        ArtistaDAO artistaDAO = (ArtistaDAO) context.getBean("artistaDAO");

        try {
            for (Artista artista:artistas) {
                artista.actuar();
                artistaDAO.insert(artista);
            }
        } catch (ActuacionException a) {
            System.out.println("Fallo en la actuación");
        }



        ((ClassPathXmlApplicationContext) context).close();
    }
/*
    public void insert() {
        JdbcTemplate jdbcTemplate;
        jdbcTemplate.update("insert into subjects (code, name, ects, grade) values (?,?,?,?)",
                subject.getCode(), subject.getName(), subject.getEcts(), subject.getGrade());
    }*/
}

