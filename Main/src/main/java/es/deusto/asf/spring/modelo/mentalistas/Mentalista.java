package es.deusto.asf.spring.modelo.mentalistas;

public interface Mentalista {
    void leerLaMente(String pensamientos);
    String getPensamientos();
}
