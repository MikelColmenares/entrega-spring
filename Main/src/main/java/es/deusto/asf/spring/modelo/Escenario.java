package es.deusto.asf.spring.modelo;

public class Escenario {
    private Escenario() {
    }

    private static class EscenarioSingletonHolder {
        static Escenario instancia = new Escenario();
    }

    public static Escenario getInstance() {
        return EscenarioSingletonHolder.instancia;
    }

    public void encenderLuces() {
        System.out.println("ESCENARIO: LUCES ENCENDIDAS");
    }

    public void apagarLuces() {
        System.out.println("ESCENARIO: LUCES APAGADAS");
    }
}
