package es.deusto.asf.spring.modelo.poemas;

public interface Poema {
    public void recitar();
}
