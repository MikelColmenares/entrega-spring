package es.deusto.asf.spring.modelo.instrumentos;

public class Piano implements Instrumento {

    @Override
    public void tocar() {
        System.out.println("PLIN PLIN PLIN");
    }
}
