package es.deusto.asf.spring.modelo.artistas;

import es.deusto.asf.spring.modelo.ActuacionException;

public interface Artista {
    public void actuar() throws ActuacionException;
    public String getNombre();
    public String getActuacion();
    public void setNombre(String nombre);
    public void setActuacion(String actuacion);
}
