package es.deusto.asf.spring.modelo.instrumentos;

public interface Instrumento {
    public void tocar();
}
