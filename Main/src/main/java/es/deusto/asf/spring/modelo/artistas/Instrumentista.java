package es.deusto.asf.spring.modelo.artistas;


import es.deusto.asf.spring.modelo.ActuacionException;
import es.deusto.asf.spring.modelo.instrumentos.Instrumento;

public class Instrumentista  extends ArtistaImpl {

    private String cancion;
    private Instrumento instrumento;

    @Override
    public void actuar() throws ActuacionException {
        System.out.println("Tocando la cancion " + cancion + ":");
        instrumento.tocar();
    }

    public String getCancion() {
        return cancion;
    }

    public void setCancion(String cancion) {
        this.cancion = cancion;
    }

    public Instrumento getInstrumento() {
        return instrumento;
    }

    public void setInstrumento(Instrumento instrumento) {
        this.instrumento = instrumento;
    }



    public Instrumentista (String nombre, String actuacion){
        super(nombre, actuacion);
    }
}
