package es.deusto.asf.spring.modelo.mentalistas;

public interface Pensador {
    void pensarSobreAlgo(String pensamientos);
}
