package es.deusto.asf.spring.modelo.dao;

import es.deusto.asf.spring.modelo.artistas.Artista;
import org.springframework.jdbc.core.JdbcTemplate;

public class ArtistaDAO implements ArtistaDAOInterf {
    JdbcTemplate jdbcTemplate;
    public ArtistaDAO(JdbcTemplate jdbcTemplate) {

        this.jdbcTemplate = jdbcTemplate;
    }
    @Override
    public void insert(Artista artista) {
        jdbcTemplate.update("insert into actuaciones (code, nombre, actuacion) values (?,?,?)",
                artista.getNombre(), artista.getActuacion());
    }
}
