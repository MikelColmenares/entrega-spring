package es.deusto.asf.spring.modelo.artistas;

public abstract class ArtistaImpl implements Artista {


    private String nombre;
    private String actuacion;

    public ArtistaImpl(String nombre, String actuacion) {
        this.nombre = nombre;
        this.actuacion = actuacion;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String getActuacion() {
        return actuacion;
    }

    public void setActuacion(String actuacion) {
        this.actuacion = actuacion;
    }
}
