package es.deusto.asf.spring.modelo.artistas;

import es.deusto.asf.spring.modelo.ActuacionException;
import es.deusto.asf.spring.modelo.poemas.Poema;

public class MalabaristaPoeta extends Malabarista {
    private Poema poema;

    public MalabaristaPoeta(String nombre, String actuacion, Poema poema) {
        super(nombre, actuacion);
        this.poema = poema;
    }


    public MalabaristaPoeta(String nombre, String actuacion, Poema poema, int bolas) {
        super(nombre, actuacion, bolas);
        this.poema = poema;
    }

    @Override
    public void actuar() throws ActuacionException {
        super.actuar();
        System.out.println("Mientras recito...");
        poema.recitar();
    }
}
